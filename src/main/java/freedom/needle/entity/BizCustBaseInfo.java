/**
 * 
 */
package freedom.needle.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author charlie
 *
 */
public class BizCustBaseInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String custId;
	private String custNumber;
	private String custName;
	private Date createTime;
	private Date updateTime;
	
	public BizCustBaseInfo() {
		super();
	}
	/**
	 * @param custId
	 * @param custNumber
	 * @param custName
	 */
	public BizCustBaseInfo(String custId, String custNumber, String custName) {
		super();
		this.custId = custId;
		this.custNumber = custNumber;
		this.custName = custName;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the custNumber
	 */
	public String getCustNumber() {
		return custNumber;
	}
	/**
	 * @param custNumber the custNumber to set
	 */
	public void setCustNumber(String custNumber) {
		this.custNumber = custNumber;
	}
	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}
	/**
	 * @param custName the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BizCustBaseInfo other = (BizCustBaseInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BizCustBaseInfo [id=" + id + ", custId=" + custId + ", custNumber=" + custNumber + ", custName="
				+ custName + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
	

}
