/**
 * 
 */
package freedom.needle.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import freedom.needle.enums.TpsInfoStateEnum;

/**
 * @author charlie
 *
 */
public class TpsInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Integer taskNo;
	private String taskName;
	private Date time;
	private BigDecimal tps;
	private Integer nodeId;
	private TpsInfoStateEnum state;
	
	public TpsInfo() {
		super();
	}
	
	/**
	 * @param taskNo
	 * @param taskName
	 * @param time
	 * @param tps
	 * @param nodeId
	 */
	public TpsInfo(Integer taskNo, String taskName, Date time, BigDecimal tps, Integer nodeId) {
		super();
		this.taskNo = taskNo;
		this.taskName = taskName;
		this.time = time;
		this.tps = tps;
		this.nodeId = nodeId;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the taskNo
	 */
	public Integer getTaskNo() {
		return taskNo;
	}
	/**
	 * @param taskNo the taskNo to set
	 */
	public void setTaskNo(Integer taskNo) {
		this.taskNo = taskNo;
	}
	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	/**
	 * @return the time
	 */
	@JsonFormat(pattern="yy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	public Date getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * @return the tps
	 */
	public BigDecimal getTps() {
		return tps;
	}
	/**
	 * @param tps the tps to set
	 */
	public void setTps(BigDecimal tps) {
		this.tps = tps;
	}
	/**
	 * @return the nodeId
	 */
	public Integer getNodeId() {
		return nodeId;
	}
	/**
	 * @param nodeId the nodeId to set
	 */
	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	/**
	 * @return the state
	 */
	public TpsInfoStateEnum getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(TpsInfoStateEnum state) {
		this.state = state;
	}

}
