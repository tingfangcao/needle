/**
 * 
 */
package freedom.needle.entity;

import java.io.Serializable;
import java.util.Date;

import freedom.needle.enums.TradeXmlStateEnum;
import freedom.needle.enums.TradeXmlStatusEnum;

/**
 * @author charlie
 *
 */
public class TradeXmlTemp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String xml;
	private TradeXmlStatusEnum status;
	private Integer retry;
	private TradeXmlStateEnum state;
	private Date createTime;
	private Date updateTime;
	
	/**
	 * 
	 */
	public TradeXmlTemp() {
		super();
	}
	/**
	 * @param xml
	 * @param status
	 * @param retry
	 * @param state
	 */
	public TradeXmlTemp(String xml, TradeXmlStatusEnum status, Integer retry, TradeXmlStateEnum state) {
		super();
		this.xml = xml;
		this.status = status;
		this.retry = retry;
		this.state = state;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the xml
	 */
	public String getXml() {
		return xml;
	}
	/**
	 * @param xml the xml to set
	 */
	public void setXml(String xml) {
		this.xml = xml;
	}
	/**
	 * @return the retry
	 */
	public Integer getRetry() {
		return retry;
	}
	/**
	 * @param retry the retry to set
	 */
	public void setRetry(Integer retry) {
		this.retry = retry;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * @return the status
	 */
	public TradeXmlStatusEnum getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(TradeXmlStatusEnum status) {
		this.status = status;
	}
	/**
	 * @return the state
	 */
	public TradeXmlStateEnum getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(TradeXmlStateEnum state) {
		this.state = state;
	}
	
	
}
