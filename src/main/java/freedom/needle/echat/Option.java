/**
 * 
 */
package freedom.needle.echat;

import java.io.Serializable;

/**
 * @author charlie
 *
 */
public class Option implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Title title;
	private Tooltip tooltip;
	private Legend legend;
	private Grid grid;
	private Toolbox toolbox;
	private XAxis xAxis;
	private YAxis yAxis;
	private Series[] series;
	
	/**
	 * @return the title
	 */
	public Title getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(Title title) {
		this.title = title;
	}
	/**
	 * @return the tooltip
	 */
	public Tooltip getTooltip() {
		return tooltip;
	}
	/**
	 * @param tooltip the tooltip to set
	 */
	public void setTooltip(Tooltip tooltip) {
		this.tooltip = tooltip;
	}
	/**
	 * @return the legend
	 */
	public Legend getLegend() {
		return legend;
	}
	/**
	 * @param legend the legend to set
	 */
	public void setLegend(Legend legend) {
		this.legend = legend;
	}
	/**
	 * @return the grid
	 */
	public Grid getGrid() {
		return grid;
	}
	/**
	 * @param grid the grid to set
	 */
	public void setGrid(Grid grid) {
		this.grid = grid;
	}
	/**
	 * @return the toolbox
	 */
	public Toolbox getToolbox() {
		return toolbox;
	}
	/**
	 * @param toolbox the toolbox to set
	 */
	public void setToolbox(Toolbox toolbox) {
		this.toolbox = toolbox;
	}
	/**
	 * @return the xAxis
	 */
	public XAxis getxAxis() {
		return xAxis;
	}
	/**
	 * @param xAxis the xAxis to set
	 */
	public void setxAxis(XAxis xAxis) {
		this.xAxis = xAxis;
	}
	/**
	 * @return the yAxis
	 */
	public YAxis getyAxis() {
		return yAxis;
	}
	/**
	 * @param yAxis the yAxis to set
	 */
	public void setyAxis(YAxis yAxis) {
		this.yAxis = yAxis;
	}
	/**
	 * @return the series
	 */
	public Series[] getSeries() {
		return series;
	}
	/**
	 * @param series the series to set
	 */
	public void setSeries(Series[] series) {
		this.series = series;
	}
	
	
}
