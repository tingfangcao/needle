/**
 * 
 */
package freedom.needle.echat;

/**
 * @author charlie
 *
 */
public class Toolbox {
	
	private Feature feature;

	/**
	 * @return the feature
	 */
	public Feature getFeature() {
		return feature;
	}

	/**
	 * @param feature the feature to set
	 */
	public void setFeature(Feature feature) {
		this.feature = feature;
	}

}
