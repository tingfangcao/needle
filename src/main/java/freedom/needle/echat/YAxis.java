/**
 * 
 */
package freedom.needle.echat;

/**
 * @author charlie
 *
 */
public class YAxis {
	
	private String type;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
