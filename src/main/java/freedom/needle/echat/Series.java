/**
 * 
 */
package freedom.needle.echat;

import java.math.BigDecimal;

/**
 * @author charlie
 *
 */
public class Series {
	
	private String name;
	private String type = "line";
	private String stack;
	private BigDecimal[] data;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the stack
	 */
	public String getStack() {
		return stack;
	}
	/**
	 * @param stack the stack to set
	 */
	public void setStack(String stack) {
		this.stack = stack;
	}
	/**
	 * @return the data
	 */
	public BigDecimal[] getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(BigDecimal[] data) {
		this.data = data;
	}

	
}
