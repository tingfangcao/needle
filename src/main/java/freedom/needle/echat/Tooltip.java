/**
 * 
 */
package freedom.needle.echat;

/**
 * @author charlie
 *
 */
public class Tooltip {
	
	private String trigger;

	/**
	 * @return the trigger
	 */
	public String getTrigger() {
		return trigger;
	}

	/**
	 * @param trigger the trigger to set
	 */
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

}
