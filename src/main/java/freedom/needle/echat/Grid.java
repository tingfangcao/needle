/**
 * 
 */
package freedom.needle.echat;

/**
 * @author charlie
 *
 */
public class Grid {
	
	private String left = "3%";
	private String right = "4%";
	private String bottom = "3%";
	private boolean containLabel = true;
	
	/**
	 * @return the left
	 */
	public String getLeft() {
		return left;
	}
	/**
	 * @param left the left to set
	 */
	public void setLeft(String left) {
		this.left = left;
	}
	/**
	 * @return the right
	 */
	public String getRight() {
		return right;
	}
	/**
	 * @param right the right to set
	 */
	public void setRight(String right) {
		this.right = right;
	}
	/**
	 * @return the bottom
	 */
	public String getBottom() {
		return bottom;
	}
	/**
	 * @param bottom the bottom to set
	 */
	public void setBottom(String bottom) {
		this.bottom = bottom;
	}
	/**
	 * @return the containLabel
	 */
	public boolean isContainLabel() {
		return containLabel;
	}
	/**
	 * @param containLabel the containLabel to set
	 */
	public void setContainLabel(boolean containLabel) {
		this.containLabel = containLabel;
	}

}
