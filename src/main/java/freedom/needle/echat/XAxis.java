/**
 * 
 */
package freedom.needle.echat;

/**
 * @author charlie
 *
 */
public class XAxis {
	
	private String type;
	private boolean boundaryGap;
	private String[] data;
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the boundaryGap
	 */
	public boolean isBoundaryGap() {
		return boundaryGap;
	}
	/**
	 * @param boundaryGap the boundaryGap to set
	 */
	public void setBoundaryGap(boolean boundaryGap) {
		this.boundaryGap = boundaryGap;
	}
	/**
	 * @return the data
	 */
	public String[] getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String[] data) {
		this.data = data;
	}
	
	

}
