/**
 * 
 */
package freedom.needle.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author charlie
 *
 */
public interface BaseDao<E> {

	public E get(Serializable identifier);

	public E find(Object primaryKey);

	public List<E> selectBySql(String sql, Object[] params);
	
	public List<E> selectBySql(String sql, Object[] params, int maxResult);
	
	public List<E> selectBySql(String sql, Object[] params, int startPosition, int maxResult);
	
	public List<E> select(String hql, Object[] params);
	
	public List<E> select(String hql, Object[] params, int maxResult);
	
	public List<E> select(String hql, Object[] params, int startPosition, int maxResult);

	/**
	 * 增改删
	 * @param sql
	 * @param params
	 * @return
	 */
	public int updateBySql(String sql, Object[] params);

	/**
	 * 增改删
	 * @param hql
	 * @param params
	 * @return
	 */
	public int update(String hql, Object[] params);

	public void save(E object);

	public void save(List<E> objects);

}
