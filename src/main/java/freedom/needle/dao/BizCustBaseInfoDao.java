/**
 * 
 */
package freedom.needle.dao;

import freedom.needle.entity.BizCustBaseInfo;

/**
 * @author charlie
 *
 */
public interface BizCustBaseInfoDao extends BaseDao<BizCustBaseInfo> {

}
