/**
 * 
 */
package freedom.needle.dao;

import freedom.needle.entity.TpsInfo;

/**
 * @author charlie
 *
 */
public interface TpsInfoDao extends BaseDao<TpsInfo> {
	
	
}
