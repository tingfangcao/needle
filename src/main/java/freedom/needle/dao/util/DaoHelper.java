/**
 * 
 */
package freedom.needle.dao.util;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.UnsupportedDataTypeException;

/**
 * @author charlie
 *
 */
public class DaoHelper {
	
	/**
	 * @param params
	 * @param stmt
	 * @throws SQLException
	 * @throws UnsupportedDataTypeException
	 */
	public static void setParams(Object[] params, PreparedStatement stmt) throws SQLException, UnsupportedDataTypeException {
		for (int i = 0; i < params.length; i++) {
			Object object = params[i];
			int parameterIndex = i+1;
			if(object == null)
				stmt.setNull(parameterIndex, Types.NULL);
			else if(object instanceof String)
				stmt.setString(parameterIndex, (String) object);
			else if(object instanceof Long)
				stmt.setLong(parameterIndex, (Long) object);
			else if(object instanceof Integer)
				stmt.setInt(parameterIndex, (Integer)object);
			else if(object instanceof Date)
				stmt.setTimestamp(parameterIndex, new java.sql.Timestamp(((Date) object).getTime()));
			else {
				//TODO
				throw new UnsupportedDataTypeException();
			}
		}
	}
	
	/**
	 * 将rs结果转换成对象列表
	 * 
	 * @param rs jdbc结果集
	 * 
	 * @param clazz 对象的映射类 return 封装了对象的结果列表
	 */
	public static <T> List<T> populate(ResultSet rs, Class<T> clazz)
			throws SQLException, InstantiationException, IllegalAccessException {
		// 结果集的元素对象
		ResultSetMetaData rsmd = rs.getMetaData();
		// 获取结果集的元素个数
		int colCount = rsmd.getColumnCount();
		// 返回结果的列表集合
		List<T> list = new ArrayList<T>();
		// 业务对象的属性数组
		Field[] fields = clazz.getDeclaredFields();
		Map<String, Field> map = new HashMap<String, Field>();
		for (int j = 0; j < fields.length; j++) {
			Field f = fields[j];
			map.put(f.getName().toUpperCase(), f);
		}

		while (rs.next()) {// 对每一条记录进行操作
			T obj = clazz.newInstance();// 构造业务对象实体
			// 将每一个字段取出进行赋值
			for (int i = 1; i <= colCount; i++) {
				Object value = rs.getObject(i);
				// 寻找该列对应的对象属性
				Field f = map.get(rsmd.getColumnName(i).toUpperCase().replaceAll("_", ""));
				if (null != f) {
					boolean flag = f.isAccessible();
					f.setAccessible(true);
					f.set(obj, value);
					f.setAccessible(flag);
				}
			}
			list.add(obj);
		}
		return list;
	}
	
	/**
	 * 对象翻译
	 * @param sourceObject
	 * @param targetClazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T> T translate(Object sourceObject, Class<T> targetClazz)
			throws InstantiationException, IllegalAccessException {
		
		Field[] targetFields = targetClazz.getDeclaredFields();
		Map<String, Field> map = new HashMap<String, Field>();
		for (int j = 0; j < targetFields.length; j++) {
			Field f = targetFields[j];
			map.put(f.getName().toUpperCase(), f);
		}

		Field[] sourceFields = sourceObject.getClass().getDeclaredFields();

		T obj = targetClazz.newInstance();
		for (int i = 1; i <= sourceFields.length; i++) {
			Field sourceField = sourceFields[i];
			Object value = sourceField.get(sourceObject);
			// 寻找该列对应的对象属性
			Field f = map.get(sourceField.getName().toUpperCase());
			if (null != f) {
				boolean flag = f.isAccessible();
				f.setAccessible(true);
				f.set(obj, value);
				f.setAccessible(flag);
			}
		}

		return obj;
	}
	
	public static Object translate(Map<String,String> relation, Map<String,Object> sourceObjectFieldValues) {
		String targetFieldName = relation.entrySet().iterator().next().getValue();
//TODO		Class.forName(className);
		
		return null;
	}
}
