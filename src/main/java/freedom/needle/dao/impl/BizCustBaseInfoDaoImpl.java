/**
 * 
 */
package freedom.needle.dao.impl;

import freedom.needle.dao.BizCustBaseInfoDao;
import freedom.needle.entity.BizCustBaseInfo;

/**
 * @author charlie
 *
 */
public class BizCustBaseInfoDaoImpl extends BaseDaoImpl<BizCustBaseInfo> implements BizCustBaseInfoDao {

	/* (non-Javadoc)
	 * @see freedom.needle.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<BizCustBaseInfo> getEntityClass() {
		return BizCustBaseInfo.class;
	}


}
