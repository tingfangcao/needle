/**
 * 
 */
package freedom.needle.dao.impl;

import freedom.needle.dao.TpsInfoDao;
import freedom.needle.entity.TpsInfo;

/**
 * @author charlie
 *
 */
public class TpsInfoDaoImpl extends BaseDaoImpl<TpsInfo> implements TpsInfoDao {

	@Override
	public Class<TpsInfo> getEntityClass() {
		return TpsInfo.class;
	}


}
