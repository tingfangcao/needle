/**
 * 
 */
package freedom.needle.dao.impl;

import freedom.needle.dao.TradeXmlTempDao;
import freedom.needle.entity.TradeXmlTemp;

public class TradeXmlTempDaoImpl extends BaseDaoImpl<TradeXmlTemp> implements TradeXmlTempDao {

	@Override
	public Class<TradeXmlTemp> getEntityClass() {
		return TradeXmlTemp.class;
	}

}
