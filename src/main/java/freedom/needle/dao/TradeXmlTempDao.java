/**
 * 
 */
package freedom.needle.dao;

import freedom.needle.entity.TradeXmlTemp;

/**
 * @author charlie
 *
 */
public interface TradeXmlTempDao extends BaseDao<TradeXmlTemp> {
	
}
