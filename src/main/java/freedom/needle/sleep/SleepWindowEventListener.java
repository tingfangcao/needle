/**
 * 
 */
package freedom.needle.sleep;

import java.util.EventListener;

/**
 * @author charlie
 *
 */
public interface SleepWindowEventListener extends EventListener {

	public void handleEvent(SleepWindowEvent event);
}
