/**
 * 
 */
package freedom.needle.sleep;

import java.util.EventObject;

/**
 * @author charlie
 *
 */
public class SleepWindowEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	private boolean open = true;

	public SleepWindowEvent(SleepWindow sleepWindow, boolean open) {
		super(sleepWindow);
		this.open = open;
	}

	/**
	 * @return the open
	 */
	public boolean isOpen() {
		return open;
	}
	
	
}
