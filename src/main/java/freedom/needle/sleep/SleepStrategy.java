/**
 * 
 */
package freedom.needle.sleep;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author charlie
 *
 */
public class SleepStrategy implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	final Lock lock = new ReentrantLock();
	
	private long time;
	private long min;
	private long max;
	private long range;
	
	public SleepStrategy() {
		super();
	}
	
	/**
	 * @param time
	 * @param min
	 * @param max
	 * @param range
	 */
	public SleepStrategy(long time, long min, long max, long range) {
		super();
		this.time = time;
		this.min = min;
		this.max = max;
		this.range = range;
	}
	
	/**
	 * 增加睡眠时间
	 */
	public void increase() {
		lock.lock();
		try {
			if(time == max)
				return;
			long temp = time + range;
			if (temp > max)
				time = max;
			else 
				time = temp;
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * 减少睡眠时间
	 */
	public void decrease() {
		lock.lock();
		try {
			if(time == min)
				return;
			long temp = time - range;
			if (temp < min)
				time = min;
			else
				time= temp;
		} finally {	
			lock.unlock();
		}
		
	}
	
	/**
	 * @return the min
	 */
	public long getMin() {
		return min;
	}
	/**
	 * @param min the min to set
	 */
	public void setMin(long min) {
		this.min = min;
	}
	/**
	 * @return the max
	 */
	public long getMax() {
		return max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(long max) {
		this.max = max;
	}
	/**
	 * @return the range
	 */
	public long getRange() {
		return range;
	}
	/**
	 * @param range the range to set
	 */
	public void setRange(long range) {
		this.range = range;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

}
