/**
 * 
 */
package freedom.needle.enums;

/**
 * @author charlie
 *
 */
public enum TradeXmlStatusEnum {
	
	STEP_1_READY,STEP_1_DOING,STEP_1_DONE,STEP_1_ERROR,
	STEP_2_READY,STEP_2_DOING,STEP_2_DONE,STEP_2_ERROR;
}

