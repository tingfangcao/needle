package freedom.needle.enums;

import freedom.needle.util.EnumDecorate;
import freedom.needle.util.EnumHelper;
import freedom.needle.util.EnumHelper.EnumKeyType;

public enum TaskCommand implements EnumDecorate<TaskCommand> {
	
	INIT(1),RUN(2),PAUSE(3),STOP(4);
	
	private Object key;
	
	private TaskCommand() {}
	
	private TaskCommand(Object key) {
		this.key = key;
	}

	@Override
	public Object getKey() {
		return key;
	}

	@Override
	public String getDisplayText() {
		return EnumHelper.display(this, EnumKeyType.CUSTOM_KEY);
	}
	
	@Override
	public TaskCommand getEnumObject(String key) {
		return EnumHelper.acquireEnumObjectByKey(TaskCommand.class, key);
	}
	
	
}
