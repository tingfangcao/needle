package freedom.needle.util;

import freedom.needle.entity.TradeXmlTemp;
import freedom.needle.queue.BlockingDistinctQueue;

public class TaskQueueFactory {
	
	private static BlockingDistinctQueue<TradeXmlTemp> ecifMessageTempQueue;
	
	public static BlockingDistinctQueue<TradeXmlTemp> getEcifMessageTempQueue() {
		if(ecifMessageTempQueue == null)
			initEcifMessageTempQueue();
		return ecifMessageTempQueue;
	}
	
	private synchronized static void initEcifMessageTempQueue() {
		if(ecifMessageTempQueue == null)
			ecifMessageTempQueue = new BlockingDistinctQueue<TradeXmlTemp>();
	}

}
