/**
 * 
 */
package freedom.needle.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author charlie
 *
 */
public class ResourceMessage {
	private static ResourceMessage resourceMessage = null;    
    private ResourceBundle resourceMessage_zh = null;    
    private ResourceBundle resourceMessage_en = null;    
    private Locale local;    
     //此时该国际化资源文件为下面的完全限定类名访问，LDP为资源文件前缀   
    private static final String SOURCE_FILE= "freedom.needle.enums.EnumsResource";   
    
    private ResourceMessage()    
    {    
        //分别将两个文件绑定到相应的语言环境下  
       resourceMessage_zh = ResourceBundle.getBundle(SOURCE_FILE, Locale.CHINA);    
       resourceMessage_en = ResourceBundle.getBundle(SOURCE_FILE, Locale.ENGLISH);    
    }    
    /**  
     * 单例模式  
     * 只创建一个实例  
     * @return  
     * @see [类、类#方法、类#成员]  
     */    
    public static ResourceMessage getInstance()    
    {    
        if (null == resourceMessage)    
        {    
            synchronized (ResourceMessage.class)    
            {    
                resourceMessage = new ResourceMessage();    
            }    
        }    
        return resourceMessage;    
    }    
        
    public String getMessage(String key)    
    {    
        return getMessage(key, local);    
    }    
        
    public String getMessage(String key, Locale local)    
    {    
        if (null == local)    
        {    
            local = Locale.getDefault();    
        }    
        String msg = null;    
        if (Locale.CHINA.equals(local))    
        {    
            msg = resourceMessage_zh.getString(key);    
        }    
        else if (Locale.ENGLISH.equals(local))    
        {    
           msg = resourceMessage_en.getString(key);    
        }    
            
        //如果找不到资源文件，返回key     
        if (null == msg || "".equals(msg))    
        {    
            return key;    
        }    
        return msg;    
    }    
    /** 测试方法  
     * @param args  
     * @see [类、类#方法、类#成员]  
     */    
    public static void main(String[] args)    
    {    
        String key = "sys.login.index";    
//        String value = ResourceMessage.getInstance().getMessage(key, Locale.ENGLISH);    
        //使用当前默认的语言环境获取，这里为中文支持  
        String value = ResourceMessage.getInstance().getMessage(key);   
        System.out.println(value);    
    }    
}
