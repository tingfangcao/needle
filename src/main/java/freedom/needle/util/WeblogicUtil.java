/**
 * 
 */
package freedom.needle.util;

/**
 * @author charlie
 *
 */
public class WeblogicUtil {
	public static final String WEBLOGIC_NODE_NAME = "weblogic.Name";
	
	private static String name;
	private static int id = 0;
	private static int nodes = 1;
	
	static {
		name = System.getProperty(WEBLOGIC_NODE_NAME);
		if(name != null) {
			String[] arr = name.split("_");
			String tail = arr[arr.length-1];
			id = Integer.valueOf(tail);
		}
	}

	/**
	 * @return the name
	 */
	public static String getName() {
		return name;
	}

	/**
	 * @return the id
	 */
	public static int getId() {
		return id;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public static void setNodes(int nodes) {
		WeblogicUtil.nodes = nodes;
	}
	
	/**
	 * @return the nodes
	 */
	public static int getNodes() {
		return nodes;
	}

}
