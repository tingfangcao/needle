/**
 * 
 */
package freedom.needle.util;

import java.util.Calendar;

/**
 * @author charlie
 *
 */
public class CalendarUtil {
	
	/**
	 * 取now的年月日，取cal的时分秒，毫秒归零
	 * @param now
	 */
	public static Calendar mixCalendar(Calendar now, Calendar cal) {
		Calendar mixCal = Calendar.getInstance();
		mixCal.setTime(now.getTime());
		mixCal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
		mixCal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
		mixCal.set(Calendar.SECOND, cal.get(Calendar.SECOND));
		mixCal.set(Calendar.MILLISECOND, 0);
		return mixCal;
	}
	/**
	 * 取now的年月日，时分秒归零，毫秒归零
	 * @param now
	 * @param tomorrow day+1
	 * @return
	 */
	public static Calendar zeroClock(Calendar now, boolean tomorrow) {
		Calendar mixCal = Calendar.getInstance();
		mixCal.setTime(now.getTime());
		if(tomorrow)
			mixCal.add(Calendar.DAY_OF_MONTH, 1);
		mixCal.set(Calendar.HOUR_OF_DAY, 0);
		mixCal.set(Calendar.MINUTE, 0);
		mixCal.set(Calendar.SECOND, 0);
		mixCal.set(Calendar.MILLISECOND, 0);
		return mixCal;
	}

}
