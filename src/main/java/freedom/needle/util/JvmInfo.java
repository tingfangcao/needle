/**
 * 
 */
package freedom.needle.util;

import java.io.Serializable;

/**
 * @author charlie
 *
 */
public class JvmInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String localHostIp;
	private int pid;
	private String localHostName;
	
	/**
	 * @return the localHostName
	 */
	public String getLocalHostName() {
		return localHostName;
	}
	/**
	 * @param localHostName the localHostName to set
	 */
	public void setLocalHostName(String localHostName) {
		this.localHostName = localHostName;
	}
	/**
	 * @return the localHostIp
	 */
	public String getLocalHostIp() {
		return localHostIp;
	}
	/**
	 * @param localHostIp the localHostIp to set
	 */
	public void setLocalHostIp(String localHostIp) {
		this.localHostIp = localHostIp;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}
	/**
	 * @param pid the pid to set
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	@Override
	public String toString() {
		return "JvmInfo [name=" + name + ", localHostIp=" + localHostIp + ", pid=" + pid + ", localHostName="
				+ localHostName + "]";
	}
	

}
