/**
 * 
 */
package freedom.needle.util;

/**
 * @author charlie
 *
 */
public interface EnumDecorate<E extends Enum<E>> {
	
	Object getKey();
	E getEnumObject(String key);
	String getDisplayText();

}
