/**
 * 
 */
package freedom.needle.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * @author charlie
 *
 */
public class XmlUtil {

	public static String genXml(Object object) throws JsonProcessingException {
		XmlMapper xmlMapper = new XmlMapper();
		String xmlStr = xmlMapper.writeValueAsString(object);
		return xmlStr;
	}
	
    public static <T> T genObjByXml(Class<T> clazz, String xmlStr) throws JsonParseException, JsonMappingException, IOException
    {
        XmlMapper xmlMapper = new XmlMapper();
        T object = xmlMapper.readValue(xmlStr, clazz);
		return object;

    }

}
