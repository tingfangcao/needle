/**
 * 
 */
package freedom.needle.util;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author charlie
 *
 */
public class JvmHelper {
	
	private static JvmInfo jvmInfo;
	
	public static JvmInfo getJvmInfo() {
		if(jvmInfo == null)
			initJvmInfo();
		return jvmInfo;
	}
	
	private synchronized static void initJvmInfo() {
		if(jvmInfo == null) {
			JvmInfo jvmInfo = new JvmInfo();
			InetAddress ia;
			try {
				ia = InetAddress.getLocalHost();
				jvmInfo.setLocalHostIp(ia.getHostAddress());
				jvmInfo.setLocalHostName(ia.getHostName());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String name = ManagementFactory.getRuntimeMXBean().getName();
			jvmInfo.setName(name);
			String pid = name.split("@")[0];
			jvmInfo.setPid(Integer.valueOf(pid));
			JvmHelper.jvmInfo = jvmInfo;
		}
	}

}
