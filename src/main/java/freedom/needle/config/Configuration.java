/**
 * 
 */
package freedom.needle.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author charlie
 *
 */
public class Configuration {
	
	private static final Logger logger = LoggerFactory.getLogger(Configuration.class);
	private static Properties config = new Properties();
	private static String[] files = {"config.properties"};
	
	private Configuration() {
		super();
	}

	private synchronized static void read() {
		if(config.isEmpty())
			for (int i = 0; i < files.length; i++) {
				InputStream is = null;
				try {
					URL url = Configuration.class.getResource(files[i]);
					is = url.openStream();
					config.load(is);
					logger.info(config.toString());
				} catch (IOException e) {
					logger.error("load properties failed!", e);
				} finally {
					try {
						if(is != null)
							is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	}

	public static Properties getConfig() {
		if(config.isEmpty())
			read();
		return config;
	}
	
	
	public static void main(String[] args) {
		Properties p = getConfig();
		System.out.println(p);
	}

}
