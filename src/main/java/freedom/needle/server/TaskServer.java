package freedom.needle.server;

public interface TaskServer {
	
	void start();
	
	void pause();
	
	void stop();
	
	void restart();

}
