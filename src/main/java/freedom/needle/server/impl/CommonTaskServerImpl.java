package freedom.needle.server.impl;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import freedom.needle.task.impl.AbstractCycleTask;

public class CommonTaskServerImpl extends AbstractTaskServer {
	
	public CommonTaskServerImpl(AbstractCycleTask[] tasks, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		super(tasks, threadPoolTaskExecutor);
	}

	@Override
	protected Class<?> getClazz() {
		return CommonTaskServerImpl.class;
	}

}
