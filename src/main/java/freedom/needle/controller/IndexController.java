/**
 * 
 */
package freedom.needle.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author charlie
 *
 */

@Controller
public class IndexController {
	
	@RequestMapping(value = { "/","/index","/welcome"})
	public ModelAndView welcomePage() {
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		return mv;
	}

}
