/**
 * 
 */
package freedom.needle.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import freedom.needle.dao.TpsInfoDao;
import freedom.needle.entity.TpsInfo;

/**
 * @author charlie
 *
 */

@Controller
public class TpsController {
	
	@Resource
	private TpsInfoDao tpsInfoDao;
	
	public static final String FRESHEST_TPSINFO = "from TpsInfo where node_id=?1 and taskNo=?2 and state=?3 order by time desc";
	
	@RequestMapping("/tps")
	public ModelAndView getIndexPage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("tps");
		return mv;
	}
	
	@RequestMapping("/tps/info")
	public @ResponseBody TpsInfo getfreshestTpsinfo(TpsInfo tpsInfo) {
		Object[] params = { tpsInfo.getNodeId(), tpsInfo.getTaskNo(), tpsInfo.getState()};
		List<TpsInfo> list = tpsInfoDao.select(FRESHEST_TPSINFO, params, 1);
		TpsInfo info = null;
		if(!list.isEmpty()) {
			info = list.get(0);
		}
		return info;
	}
	
	@RequestMapping("/tps/infos")
	public @ResponseBody TpsInfo[] getAllTaskFreshestTpsinfo(TpsInfo tpsInfo) {
		TpsInfo[] infos = new TpsInfo[3];
		for (int i = 0; i < 3; i++) {
			Object[] params = { tpsInfo.getNodeId(), i+1, tpsInfo.getState()};
			List<TpsInfo> list = tpsInfoDao.select(FRESHEST_TPSINFO, params, 1);
			TpsInfo info = null;
			if(!list.isEmpty()) {
				info = list.get(0);
			}
			infos[i] = info;
		}
		return infos;
	}
	

}
