/**
 * 
 */
package freedom.needle.mod;

import java.io.Serializable;

import freedom.needle.util.WeblogicUtil;

/**
 * @author charlie
 *
 */
public class DynamicModStrategy implements Serializable {
	private static final long serialVersionUID = 1L;
	private volatile int modValue = WeblogicUtil.getId();

	/**
	 * @return the modValue
	 */
	public int getModValue() {
		return modValue;
	}

	/**
	 * @param modValue the modValue to set
	 */
	public void setModValue(int modValue) {
		this.modValue = modValue;
	}
	
	
	
}
