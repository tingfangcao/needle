/**
 * 
 */
package freedom.needle.mod;

import freedom.needle.util.WeblogicUtil;

/**
 * @author charlie
 *
 */
public class StaticModStrategy {
	
	public static int getModValue() {
		return WeblogicUtil.getId();
	}
	
}
