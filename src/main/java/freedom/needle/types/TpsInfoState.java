/**
 * 
 */
package freedom.needle.types;

import freedom.needle.enums.TpsInfoStateEnum;

/**
 * @author charlie
 *
 */
public class TpsInfoState extends NameEnumType<TpsInfoStateEnum> {
	
	@Override
	public Class<TpsInfoStateEnum> returnedClass() {
		return TpsInfoStateEnum.class;
	}

}
