/**
 * 
 */
package freedom.needle.types;

import freedom.needle.enums.TradeXmlStateEnum;

/**
 * @author charlie
 *
 */
public class TradeXmlState extends NameEnumType<TradeXmlStateEnum> {
	
	@Override
	public Class<TradeXmlStateEnum> returnedClass() {
		return TradeXmlStateEnum.class;
	}

}
