/**
 * 
 */
package freedom.needle.types;

import freedom.needle.enums.TradeXmlStatusEnum;

/**
 * @author charlie
 *
 */
public class TradeXmlStatus extends NameEnumType<TradeXmlStatusEnum> {
	
	@Override
	public Class<TradeXmlStatusEnum> returnedClass() {
		return TradeXmlStatusEnum.class;
	}

}
