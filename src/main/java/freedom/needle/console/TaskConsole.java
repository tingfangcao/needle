/**
 * 
 */
package freedom.needle.console;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import freedom.needle.util.JvmHelper;
import freedom.needle.util.JvmInfo;

/**
 * @author charlie
 *
 */
public class TaskConsole {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private Runnable[] deamons;
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	public void start() {
		if(logger.isInfoEnabled()) {
			JvmInfo info = JvmHelper.getJvmInfo();
			logger.info(info.toString());
		}
		for (int i = 0; i < deamons.length; i++) {
			Thread t = threadPoolTaskExecutor.newThread(deamons[i]);
			threadPoolTaskExecutor.execute(t);
		}
	}

	/**
	 * @param deamons the deamons to set
	 */
	public void setDeamons(Runnable[] deamons) {
		this.deamons = deamons;
	}

	/**
	 * @param threadPoolTaskExecutor the threadPoolTaskExecutor to set
	 */
	public void setThreadPoolTaskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
		this.threadPoolTaskExecutor = threadPoolTaskExecutor;
	}

}
