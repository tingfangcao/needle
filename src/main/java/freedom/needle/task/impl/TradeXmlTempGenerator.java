package freedom.needle.task.impl;

import java.util.Date;
import java.util.UUID;

import freedom.needle.dao.TradeXmlTempDao;
import freedom.needle.entity.BizCustBaseInfo;
import freedom.needle.entity.TradeXmlTemp;
import freedom.needle.enums.TradeXmlStatusEnum;
import freedom.needle.tps.TpsWindow;
import freedom.needle.util.XmlUtil;

public class TradeXmlTempGenerator extends AbstractCycleTask {
	
	private TradeXmlTempDao tradeXmlTempDao;
	private TpsWindow tpsWindow;

	@Override
	public Class<?> getClazz() {
		return TradeXmlTempGenerator.class;
	}

	@Override
	public void execute() throws Exception {
		//生成随机客户
    	BizCustBaseInfo info = new BizCustBaseInfo();
		info.setCustNumber(UUID.randomUUID().toString());
		info.setCustName("客户" + info.getCustNumber());
		info.setCustId(String.valueOf(info.getCustNumber().hashCode()));
		info.setCreateTime(new Date());
		info.setUpdateTime(info.getCreateTime());
		//转换成XML
		String xml = XmlUtil.genXml(info);
		//构造报文对象
		TradeXmlTemp tradeXmlTemp = new TradeXmlTemp(xml, TradeXmlStatusEnum.STEP_1_READY, 0, null);
		tradeXmlTemp.setCreateTime(new Date());
		tradeXmlTemp.setUpdateTime(tradeXmlTemp.getCreateTime());
		//保存报文对象
		tradeXmlTempDao.save(tradeXmlTemp);
			
		this.sleepStrategy.decrease();
		if(tpsWindow.isOpen())
			tpsWindow.inc();
			
	}

	/**
	 * @param tradeXmlTempDao the tradeXmlTempDao to set
	 */
	public void setTradeXmlTempDao(TradeXmlTempDao tradeXmlTempDao) {
		this.tradeXmlTempDao = tradeXmlTempDao;
	}

	/**
	 * @param tpsWindow the tpsWindow to set
	 */
	public void setTpsWindow(TpsWindow tpsWindow) {
		this.tpsWindow = tpsWindow;
	}

}
