package freedom.needle.task;

public interface Task extends Runnable {
	
	void execute() throws Exception;

}
