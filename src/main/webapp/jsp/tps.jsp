<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="<c:url value='/css/bootstrap.min.css'/>">
<link rel="icon"
	href="<c:url value='/img/favicon.ico'/>">
<title><spring:message code="title" /></title>
</head>
<body>
	<div id="main" style="width: 800px;height:600px;"></div>
	<script src="<c:url value='/js/jquery-2.2.4.min.js'/>"></script>
	<script src="<c:url value='/js/bootstrap.min.js'/>"></script>
	<script src="<c:url value='/js/echarts.common.min.js'/>"></script>
	<script type="text/javascript">
		// 基于准备好的dom，初始化echarts实例
	    var myChart = echarts.init(document.getElementById('main'));

	    var option = {
	    	    title: {
	    	        text: 'Dynamic TPS',
	    	        subtext: 'beta'
	    	    },
	    	    tooltip: {
	    	        trigger: 'axis'
	    	    },
	    	    legend: {
	    	        data:['报文产生','报文抓取','报文执行']
	    	    },
	    	    toolbox: {
	    	        show: true,
	    	        feature: {
	    	            magicType: {show: true, type: ['stack', 'tiled']},
	    	            saveAsImage: {show: true}
	    	        }
	    	    },
	    	    xAxis: {
	    	        type: 'category',
	    	        boundaryGap: false,
	    	        data: []
	    	    },
	    	    yAxis: {
	    	        type: 'value',
	    	        min:0,
	    	        max:300
	    	    },
	    	    series: [{
	    	        name: '报文产生',
	    	        type: 'line',
	    	        smooth: true,
	    	        data: []
	    	    },
	    	    {
	    	        name: '报文抓取',
	    	        type: 'line',
	    	        smooth: true,
	    	        data: []
	    	    },
	    	    {
	    	        name: '报文执行',
	    	        type: 'line',
	    	        smooth: true,
	    	        data: []
	    	    }]
    	};
		
		myChart.setOption(option);
		
		var count = 60;
		var last;
		
		setInterval(function() {
			$.getJSON("<c:url value='/tps/infos'/>", {
				nodeId : "0",
				//taskNo : "1",
				state : "FRESH"
			}, function(json) {
				console.log("JSON Data: " + json);
				if(json.length && json[0].time != last) {
					var first = true;
					for (var i = 0; i < json.length; i++) {
						if(count < 0) {
							if(first)
								option.xAxis.data.shift();
							option.series[i].data.shift();
						}
						if(first)
							option.xAxis.data.push(json[i].time)
						option.series[i].data.push(json[i].tps)
						first = false;
					}
					myChart.setOption(option);
					count--;
				}
				last = json[0].time;
			});
		
		}, 3100);

		
	</script>
</body>
</html>