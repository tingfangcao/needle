<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="common.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="<c:url value='/css/bootstrap.min.css'/>">
<link rel="icon"
	href="<c:url value='/img/favicon.ico'/>">
<title><spring:message code="title" /></title>
</head>
<body>
	<strong>welcome</strong>
	
	<script src="<c:url value='/js/jquery-2.2.4.min.js'/>"></script>
	<script src="<c:url value='/js/bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(function() {

		});
	</script>
</body>
</html>