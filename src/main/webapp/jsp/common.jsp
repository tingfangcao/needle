<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%
	String scheme = request.getScheme();
	String path = request.getContextPath();
	String basePath = scheme + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	String showPath = scheme + "://www.51yunhui.com/kms/";
	String wsScheme = "http".equals(scheme) ? "ws" : "wss";
	String baseWsPath = wsScheme + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<c:set var="scheme" value="<%=scheme%>"></c:set>
<c:set var="path" value="<%=path%>"></c:set>
<c:set var="basePath" value="<%=basePath%>"></c:set>
<c:set var="showPath" value="<%=showPath%>"></c:set>
<c:set var="baseWsPath" value="<%=baseWsPath%>"></c:set>
