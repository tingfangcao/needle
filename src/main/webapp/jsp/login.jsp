<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="common.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<c:url value='/static/custom/images/favicon.ico'/>">
	<link rel="stylesheet" href="<c:url value='/static/bootstrap/css/bootstrap.min.css'/>">
	<script src="<c:url value='/static/jquery-2.2.4.min.js'/>"></script>
	<script src="<c:url value='/static/bootstrap/js/bootstrap.min.js'/>"></script>
	<title><spring:message code="title"/></title>
</head>
<body>
  <div class="container">
    <form class="form-signin" role="form" action="<c:url value='/login_check'/>" method="post">
      <h2 class="form-signin-heading">Please sign in</h2>
      <input type="username" class="form-control" name="username" placeholder="username" value="${username}" required autofocus>
      <input type="password" class="form-control" name="password" placeholder="password" value="${password}" required>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="remember-me" value="${remember-me}"> Remember me
        </label>
      </div>
      <c:if test="${msg != null}">
	      <div class="alert alert-info" role="alert">
	        <strong>${msg}</strong>
	      </div>
      </c:if>
      <c:if test="${error != null}">
	      <div class="alert alert-warning" role="alert">
	        <strong>${error}</strong>
	      </div>
      </c:if>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
  </div>

</body>
</html>