/**
 * 
 */
package needle;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import freedom.needle.entity.BizCustBaseInfo;

/**
 * @author charlie
 *
 */
public class JsonTest {

	    private JsonGenerator jsonGen = null;
	    /*
	     * Java对象转 Json
	     */
	    @Test
	    public void testGenJson()
	    {
	        ObjectMapper objMapper = new ObjectMapper();
	        Book book = new Book("Think in Java",100);
	        try {
	            jsonGen = objMapper.getFactory().createGenerator(System.out, JsonEncoding.UTF8);
	            jsonGen.writeObject(book);
	        } catch (IOException e) { 
	            e.printStackTrace();
	        } 
	    }
	    /*
	     * Json转Java对象
	     */
	    @Test
	    public void testGenObjByJson()
	    {
	        ObjectMapper objMapper = new ObjectMapper();
	        String json = "{\"name\":\"Think in Java\",\"price\":100}"; 
	        try {
	            Book book = objMapper.readValue(json, Book.class);
	            System.out.println(book);
	        } catch (IOException e) { 
	            e.printStackTrace();
	        }  
	    }
	    /*
	     * Java对象转xml
	     */
	    @Test
	    public void testGenXml()
	    {
	        XmlMapper xmlMapper = new XmlMapper();

	        Book book = new Book("Think in Java",100);
	        try {
	            String xmlStr =  xmlMapper.writeValueAsString(book);
	            System.out.println(xmlStr);
	        } catch (JsonProcessingException e) { 
	            e.printStackTrace();
	        }
	    }
	    /*
	     * xml转Java对象
	     */
	    @Test
	    public void testGenObjByXml()
	    {
	        XmlMapper xmlMapper = new XmlMapper();
	        String xmlStr = "<Book><name>Think in Java</name><price>100</price></Book>"; 
	        try {
	            Book book = xmlMapper.readValue(xmlStr, Book.class);
	            System.out.println(book);
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
	    
	    @Test
	    public void test()
	    {
	    	BizCustBaseInfo info = new BizCustBaseInfo();
			info.setCustId(String.valueOf(System.currentTimeMillis()));
			info.setCustNumber(UUID.randomUUID().toString());
			info.setCustName("客户" + info.getCustNumber());
			info.setCreateTime(new Date());
			info.setUpdateTime(info.getCreateTime());
	    	System.out.println(info);
	    }
	}